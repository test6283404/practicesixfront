import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Login from '@/views/Login.vue'
import Register from '@/views/Register.vue'
import Assets from '@/views/Assets.vue'
import Unit from '@/views/Unit.vue'
import CreateAssets from "@/views/CreateAssets.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/unit',
      name: 'unit',
      component: Unit
    },
    {
      path: '/assets',
      name: 'assets',
      component: Assets
    },
    {
      path: '/create/assets',
      component: CreateAssets
    },
  ]
})

export default router
