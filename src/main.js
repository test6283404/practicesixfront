import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import axios from "axios";

const app = createApp(App)
const backendURL = import.meta.env.VITE_AXIOS_HTTP_BASEURL
app.config.globalProperties.$http = axios;

const httpClient = axios.create({
    withCredentials: true,
    baseURL: backendURL,
});

app.use(createPinia())
app.use(router)

app.mount('#app')

export default httpClient;